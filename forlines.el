;;; forlines.el --- Loop over lines   -*- lexical-binding: t -*-

;; Copyright (C) 2020,2021 Paul Horton.
;; License: This software may be used and/or distributed under the GNU General Public License
;; Contact:  ,(concat [112 97 117 108 104 64 105 115 99 98 46 111 114 103])


;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20200329
;; Updated: 20200329
;; Version: 0.0
;; Keywords: line processing

;;; Commentary:


;;; Change Log:

;;; Code:

(require 'until)


(defun forlines-of-file-call (pathname functor &rest opts)
  "Load file PATHNAME into temporary buffer, then call `forlines-call'"
  (when (consp (car-safe opts))
    (setq opts (car opts)));  strip extraneous outer paren pair
  (with-temp-buffer
    (insert-file-contents-literally pathname)
    (forlines-call functor opts)))


(defvar forlines-call-opt-keys
  '(:skip-re :keep-len :skip-num :keep-num :text-prop?)
  "Valid keys for forline-call opts")


(defun forlines-list (&rest opts)
  "Wrapper around `forlines-call' which returns processed lines as a list.
For options OPTS, see `forlines-call'."
  (let  ((retList))
    (forlines-call
        (lambda (line)
          (push line retList))
      opts)
    (reverse retList)))


(defun forlines-call (do-each-line-functor &rest opts)
  "Call DO-EACH-LINE-FUNCTOR(line) for each line in current buffer.

OPTIONS:
  :skip-re    Ignore lines matching this regular expression at their beginning
  :keep-len   Ignore lines shorter than this length
  :skip-num   Ignore first skip-num lines that passed the :skip-re and :keep-len filters
  :keep-num   Once keep-num lines have been processed, ignore any remaining lines
  :text-prop? When true, line retains its text properties.  Defaults to false.

OPTS should be a keyword argument pair list.
If OPTS is a list of a list, the outer parens are stripped.
So that,

  (let ((X '(:keep-len 1)))
      (forlines-call do-each-line-functor X))

acts like  (forlines-call do-each-line-functor :keep-len 1).

This is useful when intermediary functions want to forward options they receive to forlines-call.

Returns the value of (funcall do-each-line-functor line))
for the last line processed;  or NIL if not lines were processed.


Caveats:
  Best to use this function under lexical binding, to make sure
  the call to 'do-each-line-functor' does not itself alter 'do-each-line-functor'

  I have not yet tested if nested calls to forlines-call work as expected or not PH20210218
"
  (declare (indent 1))
  (plist-opts/strip-extra-paren opts)
  (let (; options
        (skip-re   (plist-opts/remove opts :skip-re))
        (keep-len  (plist-opts/remove opts :keep-len))
        (skip-num  (plist-opts/remove opts :skip-num))
        (keep-num  (plist-opts/remove opts :keep-num))
        ;; other vars
        (substring-extractor (if (plist-opts/remove opts :text-prop?)
                                 #'buffer-substring #'buffer-substring-no-properties))
        (kept-count 0)
        (skpt-count 0)
        line retVal
        )
    (when opts (error "forlines-call recieved unknown options: %S" opts))
    (save-excursion
      (goto-char (point-min))
      (until (or (eobp) (equal kept-count keep-num))
        (setq line  (funcall substring-extractor (point) (point-at-eol)))
        (unless (or (and skip-re  (looking-at-p skip-re))
                    (and keep-len (< (length line) keep-len))
                    (and skip-num (<= (cl-incf skpt-count) skip-num))
                    )
          (progn
            (cl-incf kept-count)
            (setq retVal (funcall do-each-line-functor line))
            ))
        (forward-line)
        ))
    retVal))



(cl-defmacro forlines (varlist body
                               &key skip-re keep-len skip-num keep-num
                               )
  "Semi-deprecated macro.
Runs user provided code while iterating over lines in the current buffer.
See `forlines-call' for a function based implementation with similar functionality.

Run BODY code for each line in current buffer inside of a (let* (VARLIST) ...)  block.
Depending on optional parameters, some lines may be ignored.

skip-re  REGEX skips lines matching REGEX
keep-len LEN   skips lines shorter than LEN
skip-num NUM   skips first NUM lines (not counting lines filtered out by skip-re or keep-len.
keep-num NUM   return after processing NUM lines

BODY holds code to run as
 (let* VARLIST BODY)


In BODY, the variable \"line\" holds the current line.
Returns the value BODY returned on its final iteration (or nil if buffer holds no non-comment lines)


Examples:

* Push lines in buffer onto front of list L
  (forlines () ((push line L)));  Note VARLIST is mandatory even if just an empty list.


* Return sum of length of lines in buffer, skipping lines starting with \";\"
  (forlines ((len 0))  ((cl-incf len (length line))) :skip-re \";\")


* Compute sum of squared line lengths:
  (forlines  ((lenSS 0) x)  ((setq x (expt (length line) 2)) (cl-incf lenSS x)) :skip-re \";\")
"
  ;; Implementation notes.
  ;; *
  ;; The interface requires doubled outer parens in many cases, which seems a bit much.
  ;; So I experimented once (202102) with automatically adding parens for the user in some cases,
  ;; but it lead to some bugs and/or confusion, so I dropped that approach.  Paul Horton 2021.
  ;;
  ;; It might be more convenient if forlines would parse named arguments on its own
  ;; to make it easier to forward argument received via a defun (... rest& args-to-forlines)
  ;; from a caller.  Except when I tried to do this I found it really hard and gave up :-(
  ;;
  ;; Current implementation expansion always includes code such as:
  ;;
  ;;  (and ,keep-len (< (length line) ,keep-len))
  ;;
  ;; in the inner loop.
  ;; A more computation time efficient way would be to conditionally
  ;; include the check  (< (length line) ,keep-len)  in the expansion only when keep-len is non-nil
  (let
      ((kept-count  (cl-gensym))
       (skpt-count  (cl-gensym))
       (retVal      (cl-gensym))
       )
    `(let* (,kept-count
            ,skpt-count
            ,retVal
            line;  intentionally visible in user provided BODY
            ,@varlist
            )
       (setq ,kept-count 0
             ,skpt-count 0
             )
       (save-excursion
         (goto-char (point-min))
         (while (and (not (eobp))
                     (if ,keep-num
                         (< ,kept-count ,keep-num)
                       t))
           (setq line  (buffer-substring-no-properties (point) (point-at-eol)))
           (if (or (and ,skip-re  (looking-at-p ,skip-re))
                   (and ,keep-len (< (length line) ,keep-len))
                   (and ,skip-num (<= (cl-incf ,skpt-count) ,skip-num))
                   )
               nil
             (progn
               (cl-incf ,kept-count)
               (setq ,retVal (progn ,@body))
               ))
           (forward-line 1)
           )
         ,retVal
         ))))


(provide 'forlines)

;;; forlines.el ends here

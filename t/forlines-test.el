;;; forlines-test.el ---   -*- lexical-binding: t -*-

;; Copyright (C) 2021, Paul Horton, All rights reserved.
;; License: This software may be used and/or distributed under the GNU General Public License
;; Contact:  ,(concat [112 97 117 108 104 64 105 115 99 98 46 111 114 103])

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20210205
;; Updated: 20210205
;; Version: 0.0
;; Keywords: 

;;; Commentary:

;;; Change Log:

;;; Code:


(ert-deftest forlines-call-test--basic ()
  (with-current-buffer
      "forlines-data"
    (should
     (equal (reverse '("4282" "4341"))
            (let ((L))
              (forlines-call
               (lambda (line) (push line L))
               :skip-re "[#]" :keep-len 1 :skip-num 1 :keep-num 2)
            )))))

(ert-deftest forlines-call-test--opts-from-listvar ()
  (with-current-buffer
      "forlines-data"
    (should
     (equal (reverse '("4282" "4341"))
            (let (L
                  (opts '(:skip-re "[#]" :keep-len 1 :skip-num 1 :keep-num 2))
                  )
              (forlines-call
               (lambda (line) (push line L))
               opts)
            )))))

(ert-deftest forlines-call-test--sum-squares ()
  (with-current-buffer
      "forlines-data"
    (should
     (equal 1137
            (let ((lenSS 0))
              (forlines-call
               (lambda (line) (cl-incf lenSS (expt (length line) 2)))
               :skip-re "#"))
            ))
            ))


(ert-deftest forlines-call-test--text-prop ()
  (with-current-buffer
      "forlines-data"
    (set-text-properties (point-min) (point-max) '(:forlines-test :a))
    (should
     (equal
      :a
      (get-text-property 0 :forlines-test
                         (forlines-call  (lambda (line) line)
                                         :text-prop? t :skip-re "#")
      )))))
  

(ert-deftest forlines-test ()
  (with-current-buffer
      "forlines-data"
    (should
     (equal (reverse '("4282" "4341"))
            (forlines ((L nil)) ((push line L))
                      :skip-re "[#]" :keep-len 1 :skip-num 1 :keep-num 2)
            ))
    (should
     (equal 1137
            (forlines  ((lenSS 0) x)    ((setq x (expt (length line) 2)) (cl-incf lenSS x)) :skip-re "#")
            ))
    ))


;;; forlines-test.el ends here
